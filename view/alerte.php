<?php
require '../php/affichage.php';
session_start();
if(!isset($_SESSION) || $_SESSION['connected'] !== 1) {
    header("Location: connexion.php");
    
} 
if(!empty($_GET['alerte']) && isset($_GET['alerte'])){
    $alerte=$_GET['alerte'];
    $updateAlerte=$db->query("UPDATE probleme Set envoie='1' WHERE idProbleme=$alerte");
}
$i=0;
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <link rel="stylesheet" href="../style/pimp.css" type="text/css">
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart Lighting</title>
</head>
<body>
<?php include "nav.php";?>

    <div class="encadrer">
    <h1 class="title">Alerte en cours</h1>
    <div class="container">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Site</th>
                    <th scope="col">Type</th>
                    <th scope="col">Date</th>
                    <th scope="col">vent</th>
                    <th scope="col">Intensite</th>
                    <th scope="col">Commentaire</th>
                    <th scope="col">Photo</th>
                </tr>
            </thead>
            <tbody>
                <?php while($alerteNowExe=$alerteNow->fetch()):
                    if($alerteNowExe['flag']==0): ?>
                    <tr class="table-active">
                        <td ><?= $alerteNowExe['nomSite']; ?></td>
                        <td><?= $alerteNowExe['type']; ?></td>
                        <td><?= $alerteNowExe['dateProbleme']; ?></td>
                        <td><?= $alerteNowExe['vent']; ?></td>
                        <td><?= $alerteNowExe['electricite']; ?></td>
                        <td><?= $alerteNowExe['commentaire']; ?></td>
                        <?php if(empty($alerteNowExe['urlProbleme'])):?>
                        <td></td>
                        <?php else: ?>
                        <td ><a href="../imageCom/<?= $alerteNowExe['urlProbleme']; ?>"><img id="imgAlerte" src="../imageCom/<?= $alerteNowExe['urlProbleme']; ?>"></a></td>                      
                        <?php endif;?>
                        <td><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="<?= '#'.$tabSemaine[$i]?>">Probleme réparé</button></td>
                            <div class="modal fade" id="<?= $tabSemaine[$i]?>" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <h4>L'anomalie  est réparée</h4>
                                            <form method="POST" action="">
                                            <?php include '../php/updateAlerte.php'; ?>
                                                <div class="form-group" id="form-hidden">
                                                    <label class="col-form-label mt-4" for="id">Date de pose</label>
                                                    <input type="text" class="form-control" id="id" name="id" value="<?=$alerteNowExe['idProbleme']?>" required>
                                                </div>
                                                <input type="submit" name="submit" id="connexion" class="btn btn-success" data-bs-dismiss="modal" value="OK">    
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                    </tr>
                <?php $i++; endif; endwhile;?>
            </tbody>
        </table>
        <input type="button" class="btn btn-success mt-4" onclick='window.location.reload(false)' value="Mettre a jour les alertes">
    </div>
    </div>
</body>
</html>

<script src="../style/jquery.js"></script>
<script src="../vendor/bootstrap.min.js"></script>
<script src="../vendor/axio.min.js"></script>
<script src="../vendor/moment.min.js"></script>


