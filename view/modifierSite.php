<?php session_start();

if(!isset($_SESSION) || $_SESSION['connected'] !== 1) {
    header("Location: connexion.php");
    
} 
require '../php/affichage.php';
require '../php/updateSite.php'
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
        <link rel="stylesheet" href="../style/pimp.css" type="text/css">
        <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
        <title>Smart Lighting</title>
    </head>
    <body>
    <?php include "nav.php";?>
    <div class="container">
        
        <fieldset>
            <legend>Paramètre</legend>
            <form method="POST" action="" enctype="multipart/form-data">
                
                <div class="form-group">
                    <label for="site" class="col-form-label label mt-4 ">Quel site voulez vous modifier?</label>
                    <select onchange="changeSite()" class="form-select form-control" id="site" name="site">  
                        <?php while ($recupAllSiteExe=$recupAllSite->fetch()):?>
                            <option value=<?php echo $recupAllSiteExe['codeAffaire']?>><?php echo $recupAllSiteExe['nomSite']?></option>
                        <?php endwhile;?>
                    </select>
                </div>
                <div id="changeHidden">
                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="nomSite">Nom du site</label>
                        <input type="text" class="form-control"  id="nomSite" name="nomSite" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="adress">Adresse</label>
                        <input type="text" class="form-control"  id="adress" name="adress" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="city">Ville</label>
                        <input type="text" class="form-control"  id="city" name='city' required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="lat">Latitude</label>
                        <input type="text" class="form-control"  id="lat" name="lat" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="long">Longitude</label>
                        <input type="text" class="form-control"  id="long" name="long" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label label mt-4" for="start">Date de début</label>
                        <input type="date" class="form-control" id="start" name="start" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label label mt-4" for="end">Date de fin</label>
                        <input type="date" class="form-control" id="end" name="end" required>
                    </div>

                    
                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="newHabillage">Nouvel habillage</label>
                        <input type="date" class="form-control" id="newHabillage" name="newHabillage" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="poseur">Poseur</label>
                        <input type="text" class="form-control"  id="poseur" name="poseur" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="imprimeur">Imprimeur</label>
                        <input type="text" class="form-control"  id="imprimeur" name="imprimeur" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="seuilIntensite">Seuil d'intensité</label>
                        <input type="text" class="form-control"  id="seuilIntensite" name="seuilIntensite" >
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="seuilVent">Seuil du vent</label>
                        <input type="text" class="form-control"  id="seuilVent" name="seuilVent" >
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="seuilLumHaut">Seuil haut de luminosité </label>
                        <input type="text" class="form-control"  id="seuilLumHaut" name="seuilLumHaut" >
                    </div>

                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="seuilLumBas">Seuil haut de luminosité </label>
                        <input type="text" class="form-control"  id="seuilLumBas" name="seuilLumBas" >
                    </div>

                    
                    <div class="form-group">
                        <label class="col-form-label mt-4 label" for="seuilTensionFil">Seuil de tension du fil </label>
                        <input type="text" class="form-control"  id="seuilTensionFil" name="seuilTensionFil" >
                    </div>

                    <div class="envoyer">
                        <button type="submit" class="btn btn-success sub" name="submit">Ajouter</button>
                    </div>
                </div>

            </form>
            
        </fieldset>
        
        
    </body>
</html>

<script src="../style/jquery.js"></script>
<script>
    document.querySelector('#site').addEventListener("change", function() {
        const v = this.value;
        const formAnnonceur = document.getElementById('changeHidden');

        formAnnonceur.style.display = "block";
    });

    function changeSite() {

        site = document.getElementById("site")

        const nomSite = document.getElementById('nomSite')
        const adress = document.getElementById('adress')
        const city = document.getElementById('city')
        const lat = document.getElementById('lat')
        const long = document.getElementById('long')
        const start = document.getElementById('start')
        const end = document.getElementById('end')
        const poseur = document.getElementById('poseur')
        const imprimeur = document.getElementById('imprimeur')
        const seuilIntensite = document.getElementById('seuilIntensite')
        const seuilVent = document.getElementById('seuilVent')
        const seuilLumHaut    = document.getElementById('seuilLumHaut')
        const seuilLumBas     = document.getElementById('seuilLumBas')
        const seuilTensionFil = document.getElementById('seuilTensionFil')
        const newHabillage = document.getElementById('newHabillage')

        $.ajax({
            url: "../php/ajax/site.php",
            method: 'GET',
            data: "id=" + site.value,
            success: function (event) {
                
                event = JSON.parse(event);
                console.log(event)
                // for (let i = 0; i < event.length; i++) {
                //     select.append(
                //         `<option value="${event[i]['activite']}">${event[i]['activite']}</option>`)
                // }
                // typeof event === "object" && event.length !== 0
                
                nomSite.value=event[0]['nomSite']
                adress.value=event[1]['adress']
                city.value=event[2]['city']
                var dateStart = event[3]['start'].split(" ")
                start.value=dateStart[0]
                long.value=event[4]['long']
                lat.value=event[5]['lat'] 
                var dateEnd = event[6]['end'].split(" ")
                end.value = dateEnd[0]
                var dateHabillage = event[7]['newHabillage'].split(" ")
                newHabillage.value=dateHabillage[0]
                seuilIntensite.value=event[8]['seuilIntensite']
                seuilVent.value=event[9]['seuilVent']
                seuilLumHaut   .value=event[10]['seuilLumHaut']
                seuilLumBas    .value=event[11]['seuilLumBas']
                seuilTensionFil.value=event[12]['seuilTensionFil']
                poseur.value=event[13]['poseur']
                imprimeur.value=event[14]['imprimeur']

            },
            error: function (err) {
                console.error(err)
                console.log("error")
            }
        })
    }
</script> 
