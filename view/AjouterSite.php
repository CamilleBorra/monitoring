<?php session_start();

require_once '../php/affichage.php';
// if(!isset($_SESSION) || $_SESSION['connected'] !== 1) {
//     header("Location: connexion.php");
    
// } ?>
<?php // ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
        <link rel="stylesheet" href="../style/pimp.css" type="text/css"> 
        <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'>
        <title>Smart Lighting</title>
    </head>
    <body>
    <?php include "nav.php";?>
    <div class="container">
        <fieldset>
            <legend>Quel site voulez vous ajouter?</legend>
            <form method="POST" action="" enctype="multipart/form-data">
            <?php require '../php/insertSite.php'; ?>
                <div class="form-group">
                    <label for="agence" class="col-form-label mt-4 label">Agence</label>
                    <select class="form-select form-control" id="agence" name="agence">  
                        <?php while ($agenceExe=$agence->fetch()):?>
                            <option value=<?php echo $agenceExe['idAgence']?>><?php echo $agenceExe['nameAgence']?></option>
                        <?php endwhile;?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="site">Nom du site</label>
                    <input type="text" class="form-control"  id="site" name="site" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="adress">Adresse</label>
                    <input type="text" class="form-control"  id="adress" name="adress" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="city">Ville</label>
                    <input type="text" class="form-control"  id="city" name='city' required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="lat">Latitude</label>
                    <input type="text" class="form-control"  id="lat" name="lat" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="long">Longitude</label>
                    <input type="text" class="form-control"  id="long" name="long" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4" for="start">Date de début</label>
                    <input type="date" class="form-control" id="start" name="start" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4" for="end">Date de fin</label>
                    <input type="date" class="form-control" id="end" name="end" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="poseur">Poseur</label>
                    <input type="text" class="form-control"  id="poseur" name="poseur" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="imprimeur">Imprimeur</label>
                    <input type="text" class="form-control"  id="imprimeur" name="imprimeur" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="seuilIntensite">Seuil d'intensité</label>
                    <input type="text" class="form-control"  id="seuilIntensite" name="seuilIntensite" >
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="seuilVent">Seuil du vent</label>
                    <input type="text" class="form-control"  id="seuilVent" name="seuilVent" >
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="seuilLumHaut">Seuil haut de luminosité </label>
                    <input type="text" class="form-control"  id="seuilLumHaut" name="seuilLumHaut" >
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="seuilLumBas">Seuil haut de luminosité </label>
                    <input type="text" class="form-control"  id="seuilLumBas" name="seuilLumBas" >
                </div>

                
                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="seuilTensionFil">Seuil de tension du fil </label>
                    <input type="text" class="form-control"  id="seuilTensionFil" name="seuilTensionFil" >
                </div>

                <div class="form-group form-photo">
                    <label class="label" for="images">Choisir une image:</label>
                    <input class="form-control" type="file" id="images" name="images" accept="image/jpg" required>
                </div>

                <div class="envoyer">
                    <button type="submit" class="btn btn-success sub" name="submit">Ajouter</button>
                </div>

            </form>
        </fieldset>                   
    </body>
</html>
