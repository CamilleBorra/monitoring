<?php require_once '../php/affichage.php';
 session_start();?>
<?php  $today = date('Y-m').'-01';
$timer=strtotime($today);
//$dateStart=$today+'01';
$dateStart=date('Y-m-d', strtotime("+1 days", $timer));

$timesta=strtotime($today);
$dateStartDate=date('Y-m-d',$timesta);
$lastMonth=date('Y-m-d', strtotime('+2 month',$timesta));
//$lastMonth=$today+1;
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <link rel="stylesheet" href="../style/pimp.css" type="text/css"> 
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart Lighting</title>
</head>
<body>
    <?php
    include "nav.php";
    $y=0;?>
    <div class="encadrer">
    <h1 class="title">Planning</h1>
 
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Site</th>
                        <th scope="col">Imprimeur</th>
                        <th scope="col">Poseur</th>
                        <?php for($i=0;$i<62;$i++):?>
                        <th  style="width:10px;"></th>
                        <?php endfor;?>
                    </tr>
                </thead>
                <tbody>
                    <?php while($sitesExe=$sites->fetch()):?>
                    <tr class="table-active">
                        <td  id="site" class="cliqueLien" onclick="info('<?= $sitesExe['codeAffaire'] ?>')"><?= $sitesExe['nomSite']; ?></td>
                        <td  id="imprimeur"><?= $sitesExe['imprimeur']; ?></td>
                        <td id="poseur"><?= $sitesExe['poseur']; ?></td>
                        <?php $id=$sitesExe['codeAffaire'];
                        $planning=$db->query("SELECT datePose,dateDepose,labelAnnonceur,color,idInstallation From  annonceur a, installation i WHERE  i.idAnnonceur=a.idAnnonceur AND codeAffaire=$id");
                        
                        $fetchPlanning = $planning->fetchAll(PDO::FETCH_ASSOC);

                        /**
                         * Iterate days (60 days)
                         */
                        for ($i=0; $i < 62; $i++) { 
                            $output = '<td>';
                            for ($a=0; $a < count($fetchPlanning); $a++) { 
                                $pla = $fetchPlanning[$a];

                                $datePose       = $pla['datePose'];
                                $datePoseForm   = date('Y-m-d', strtotime($datePose));
                                $datePoseDate   = date('d-m-Y', strtotime($datePose));
                                $dateDepose     = $pla['dateDepose'];
                                $dateDeposeForm = date('Y-m-d', strtotime($dateDepose));
                                $dateDeposeDate = date('d-m-Y', strtotime($dateDepose));
                                $color          = $pla['color'];
                                $annonceur      = $pla['labelAnnonceur'];

                                // echo $datePose . ' ' . $dateDepose . ' ' . $color . ' ' . $annonceur . PHP_EOL;

                                if($datePose <= date('Y-m-d', strtotime("+$i days", $timesta)) && $dateDepose >= date('Y-m-d', strtotime("+$i days", $timesta))) {
                                    $output .= '<div data-bs-toggle="modal" data-bs-target="#S'.$pla['idInstallation'].'" style="background-color:' . $color . '; margin:0 -10px;padding:10px;width:10px; color:white;" title="'. $annonceur .'
'.$datePoseDate.' 
'.$dateDeposeDate.'">' . '</div>';
                                }?>
                                <div class="modal fade" id="<?= 'S'.$pla['idInstallation']?>" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel2"><?= $pla['labelAnnonceur'];?></h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="POST" action="">
                                                    <?php include '../php/updatePlanning.php'; ?>
                                                    <div class="form-group" id="form-hidden">
                                                        <label class="col-form-label mt-4" for="id">Date de pose</label>
                                                        <input type="text" class="form-control" id="id" name="id" value="<?=$pla['idInstallation']?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-form-label mt-4" for="start">Date de pose</label>
                                                        <input type="date" class="form-control" id="start" name="start" value="<?=$datePoseForm?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-form-label mt-4" for="end">Date de dépose</label>
                                                        <input type="date" class="form-control" id="end" name="end" value="<?=$dateDeposeForm?>"required>
                                                    </div>
                                                    <input type="submit" name="submitModify" id="connexion" class="btn btn-success mt-4" data-bs-dismiss="modal" value="Valider">    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <?php

                            }
                            $output .= '</td>';
                            echo $output; ?>

                                
                            <?php
                        }
                        
                        ?>
                        
                    </tr>
                    <?php $y=$y+1; endwhile; ?>
                </tbody>
            </table>
    
    <script src="../style/jquery.js"></script>
    <script>
        function info(site){
            window.location.href = 'tabPlanning.php?site='+site
        }
    </script>
    <button class="btn btn-success mt-4"><a href="formPlanning.php">Ajouter une pose</a></button>
    <input type="button" class="btn btn-success mt-4" onclick='window.location.reload(false)' value="Mettre a jour le planning">
    </div>
</body>
</html>
<script src="../style/jquery.js"></script>
<script src="../vendor/bootstrap.min.js"></script>
<script src="../vendor/axio.min.js"></script>
<script src="../vendor/moment.min.js"></script>