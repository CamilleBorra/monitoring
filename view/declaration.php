<?php require_once '../php/affichage.php';
 session_start();?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <link rel="stylesheet" href="../style/pimp.css" type="text/css">
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart Lighting</title>
</head>
<body>
    <?php include "nav.php";?>
<div class="container">
       <fieldset>   
            <legend>Rapport</legend>
            <form method="POST" action="" enctype="multipart/form-data">
            <?php include '../php/insertDeclaration.php'; ?>
                
                <div class="form-group">
                    <label for="site" class="form-label label mt-4">Site - Campagne en cours</label>
                    <select class="form-select" id="site" name="site">                    
                        <?php while ($recupAllSiteExe=$recupCampagneSite->fetch()):?>
                            <option value="<?= $recupAllSiteExe['codeAffaire']?>"><?php echo $recupAllSiteExe['nomSite'].' - '.$recupAllSiteExe['labelAnnonceur']?></option>
                        <?php endwhile;?>
                        </select>                    
                </div>
                <div class="form-group">
                    <label for="type" class="form-label label mt-4">Type de probleme</label>
                    <select class="form-select" id="type" name="type">                                            
                        <option value="4">Lampe éteinte</option>
                        <option value="5">Vandalisme</option>
                        <option value="6">Probleme d'horaire</option>
                        <option value="8">Reparation d'anomalie</option>

                    </select>                    
                </div>
                <div class="form-group" >
                    <label for="contenu" class="form-label label mt-4">Commentaire</label>
                    <textarea class="form-control" name="contenu" id="contenu" rows="3" value="contenu" placeholder="Commentaire..." required></textarea>
                </div>
                <div id="form-camera" class="form-group label mt-4">
                    <label for="file">Prendre une photo</label>
                    <input type="file" id="file" name="file" accept="image/jpg" required>
                </div>                       
                <div class="envoyer mt-4">
                    <button type="submit" class="btn btn-success" name="submit">Valider</button>
                </div>
            </form>
        </fieldset>
    </div>
</body>
</html>