<?php session_start();
if(!isset($_SESSION) || $_SESSION['connected'] !== 1) {
    header("Location: connexion.php");
    
} 
require_once "../php/bdd.php";
require_once "../php/affichage.php";
require "../php/deleteSite.php"

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
        <link rel="stylesheet" href="../style/pimp.css" type="text/css">
        <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
        <title>Smart Lighting</title>
    </head>
    <body>
    <?php include "nav.php";?>
        <div class="container">
            <fieldset>
                <legend>Quel site voulez vous supprimer?</legend>
                <form method="POST" action="" >
                    
                    <div class="form-group">
                        <label for="site" class="col-form-label mt-4 label">Site</label>
                        <select class="form-select form-control" id="site" name="site">
                            <option value="default"></option>
                            
                            <?php while ($donnee=$sites->fetch()):?>
                                <option value=<?php echo $donnee['codeAffaire']?>><?php echo $donnee['nomSite']?></option>
                            <?php endwhile;?>
                            
                        </select>
                    </div>
                    <div class="envoyer mt-4">
                        <button type="submit" class="btn btn-success sub" name="submit">Supprimer</button>
                    </div>
                    <input type="button" class="btn btn-success mt-4" onclick='window.location.reload(false)' value="Mettre a jour la liste">
                </form> 
            </fieldset>
        </div>   
    </body>
</html>
