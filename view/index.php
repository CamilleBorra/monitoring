<?php // session_start();
//if(!isset($_SESSION) || $_SESSION['connected'] !== 1) {
    //header("Location: connexion.php");
    
//} 
    require_once "../php/affichage.php";
    session_start();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>
   <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <!-- <link rel="stylesheet" href="../style/style.css" type="text/css">  -->
    <link rel="stylesheet" href="../style/pimp.css" type="text/css">
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
   
   <script src="https://kit.fontawesome.com/900a31a894.js" crossorigin="anonymous"></script>
   
    <title>Smart Lighting</title>
</head>
<body>
    <?php include "nav.php";?>
    <div id="corp">
        <div id="map"></div>    
        <div id="reglage">
            <div id="bandeau">
                <img src="../style/image/logo.png">
                <h1>Smart Lighting</h1>
            </div>
            <div id="under-reglage">
                <div id="ville">
                    <?php while($agenceExe=$agence->fetch()): ?>
                    <input type="button" class="btn btn-primary btn-lg zoom" value="<?=$agenceExe['nameAgence']?>" />
                    <?php endwhile ?>
                </div>
                <div id="etat">
                    <div onclick="window.location.href='?etat=all'" id="all">
                        <input type="button" class="inputNumber"  value=Tout >                   
                    </div>
                    <div onclick="window.location.href='?etat=ok'" id="ok">
                        <input type="button" class="inputNumber"  value=0 >
                        <img class="pointLogo" src="../style/image/Vert.png">
                    </div>
                    <div onclick="window.location.href='?etat=probleme'" id="probleme">
                        <input type="button" class="inputNumber"  value=0 >
                        <img class="pointLogo" src="../style/image/Rouge.png">
                    </div>
                    <div onclick="window.location.href='?etat=no'" id="no">
                        <input type="button" class="inputNumber"  value=0 >
                        <img class="pointLogo" src="../style/image/Jaune.png">
                    </div>
                </div>
                <div id="redirection">
                    <button class="btn btn-primary btn-lg" ><a href="planning3.php">Voir le planning</a></button>
                    <button class="btn btn-primary btn-lg" ><a href="declaration.php">Déclarer un incident</a></button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>
   <script src="../style/jquery.js"></script>
    <script>
function isArray(e) {
return typeof e === "object" && e.length !== undefined
}
function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}

       var map = L.map('map').setView([48.8588897, 2.320041], 11);
        $('.zoom').click(function () {
            let agence = $(this).attr('value')
            
            $.ajax({
                method: 'GET',
                url: `../php/ajax/map.php?agence=${agence}`,
                success: function(response) {
                    console.log(typeof response, response)
                    var data = JSON.parse(response)
                    map.setView([data[0],data[1]], 11)  
                    
                },
                error: function(error) {
                    throw error
                }
            })
        })
        
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiY2FtaWxsZWJvciIsImEiOiJja3l2cDZzY2QwMHNrMnhwZ3JobWRxZWtvIn0.esgpYy9dJ5z62iSHZXwyrA', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
    }).addTo(map);
    
    var iconJaune=L.icon({
        iconUrl: '../style/image/Jaune.png',
        iconSize: [45, 45],
        popupAnchor: [0,-20],
    });

    var iconVerte=L.icon({
        iconUrl: '../style/image/Vert.png',
        iconSize: [45, 45],
        popupAnchor: [0, -20],
    });
    
    var iconRouge=L.icon({
        iconUrl: '../style/image/Rouge.png',
        iconSize: [35, 55],
        popupAnchor: [0, -20],
    });

    var iconNoir=L.icon({
        iconUrl: '../style/image/marqueur_noir.png',
        iconSize: [45, 45],
        popupAnchor: [0, -20],
    });
    var index=["One","Two","Three", "Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen","Twenty","Twenty-one","Twenty-two","Twenty-three","Twenty-four","Twenty-five","Twenty-six","Twenty-seven","Twenty-eight","Twenty-nine","Thirty","Thirty-one","Thirty-two","Thirty-three","Thirty-four","Thirty-five","Thirty-six","Thirty-seven","Thirty-eight","Thirty-nine","Forty","Forty-one","Forty-two","Forty-three","Forty-four","Forty-five","Forty-six","Forty-seven","Forty-eight","Forty-nine","Fifty","Fifty-one","Fifty-two"];
   
    var etat
    if($_GET('etat')=='ok'||$_GET('etat')=='probleme'||$_GET('etat')=='no'){
        etat=$_GET('etat')
    }
    else{
        etat='all'
        
    }
    
    // $('.inputNumber').click(function (){
    //         et=$(this).attr('id');
    //         window.location.href = `?etat=${et}`
    //     })
    
    $.ajax({
        
        url: "../php/ajax.php",
        method: 'GET',
        success: function(event) { 
           
            const obj= JSON.parse(event)
            // console.log('toto')
            // console.log(obj)
           
            var ok=0
            var probleme=0
            var no=0
            var today = new Date()
            var month = today.getMonth()<10 ? '0'+today.getMonth() : today.getMonth()
            today = today.getFullYear()+'-'+month+'-'+today.getDate()+' 00:00:00'
            for(var i=0; i<obj.length;i++){ 
                if(obj[i]['debut']>today){
                    if(etat=='all'|| etat=='no'){
                        index[i] =L.marker([obj[i]['lattitude'],obj[i]['longitude']], {icon: iconJaune}).addTo(map);
                        index[i].bindPopup(
                                    '<h1 class="name">'
                                        +obj[i]['nom']
                                    +'</h1>'
                                    +'Début du dispositif: '
                                    +obj[i]['debut']
                                    +'</br>'
                                    +'Fin du dispositif: '
                                    +obj[i]['fin']

                                );
                    }
    
                    no+=1
                }
                else{
                    if(obj[i]['categorie']==='ok'|| obj[i]['flag']==1){

                        if(etat=='all'|| etat=='ok'){
                            index[i] =L.marker([obj[i]['lattitude'],obj[i]['longitude']], {icon: iconVerte}).addTo(map);
                            index[i].bindPopup(
                                        '<h1 class="name">'
                                            +obj[i]['nom']
                                        +'</h1>'
                                        +'Actuellement '
                                        +obj[i]['categorie']
                                        +'</br>'
                                        +'Vent: '
                                        +obj[i][0]['vent']
                                        +'km/h'
                                        +'</br>'
                                        +'Intensite: '
                                        +obj[i][0]['intensite']
                                        +'</br>'
                                        +'Début du dispositif: '
                                        +obj[i]['debut']
                                        +'</br>'
                                        +'Fin du dispositif: '
                                        +obj[i]['fin']

                                    );
                        }                    
                    ok+=1
                    }
                    else{

                        if(etat=='all'|| etat=='probleme'){
                            index[i]=L.marker([obj[i]['lattitude'],obj[i]['longitude']], {icon: iconRouge}).addTo(map);
                            index[i].bindPopup(
                                        '<h1 class="name">'
                                            +obj[i]['nom']
                                        +'</h1>'
                                        +'Actuellement '
                                        +obj[i]['categorie']
                                        +'</br>'
                                        +'Vent: '
                                        +obj[i][0]['vent']
                                        +'km/h'
                                        +'</br>'
                                        +'Intensite: '
                                        +obj[i][0]['intensite']
                                        +'</br>'
                                        +'Début du dispositif: '
                                        +obj[i]['debut']
                                        +'</br>'
                                        +'Fin du dispositif: '
                                        +obj[i]['fin']

                                    );
                                    
                        }

                    probleme+=1

                    }
                }
                
            }

            
            $('#ok > input').val(ok)
            $('#probleme > input').val(probleme)
            $('#no > input').val(no)

            // document.getElementById('ok').value=ok
            // document.getElementById('probleme').value=probleme
        },
        error: function(err) {
            
            console.error(err)
            console.log("error")
        }
   })

  
        

    </script>
    
    
</body>
</html>