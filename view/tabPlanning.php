<?php 
require_once "../php/affichage.php";
session_start();
if(isset($_GET['site'])&&!empty($_GET['site'])){
    $site=$_GET['site'];
    $tabPose=$db->query("SELECT datePose,dateDepose,labelAnnonceur,nomSite,idInstallation FROM annonceur a, installation i,site s WHERE a.idAnnonceur=i.idAnnonceur AND i.codeAffaire=s.codeAffaire AND s.codeAffaire=\"$site\"");
    $nomSite=searchNomSite($site,$db);

}
$i=0;
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <link rel="stylesheet" href="../style/pimp.css" type="text/css">
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart Lighting</title>
</head>
<body>
<?php include "nav.php";?>
    <div class="encadrer">
    <h1 class=title><?=$nomSite?></h1>
    <div class="container">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Annonceur</th>
                    <th scope="col">Date de pose</th>
                    <th scope="col">Date de depose</th>
                </tr>
            </thead>
            <tbody>
                <?php while($tabPoseExe=$tabPose->fetch()): 
                    $datePoseDate=date('d-m-Y', strtotime($tabPoseExe['datePose']));
                    $datePoseForm=date('Y-m-d', strtotime($tabPoseExe['datePose']));
                    $dateDeposeDate=date('d-m-Y', strtotime($tabPoseExe['dateDepose']));
                    $dateDeposeForm=date('Y-m-d', strtotime($tabPoseExe['dateDepose']));?>
                    <tr class="table-active">
                        <td><?= $tabPoseExe['labelAnnonceur']; ?></td>
                        <td><?= $datePoseDate; ?></td>
                        <td><?= $dateDeposeDate; ?></td>
                        <td><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="<?= '#'.$tabSemaine[$i]?>">Modifier</button></td>

                           
                        <td><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="<?= '#'.$tabSemaine[$i]?>">Supprimer</button></td>
                        <div class="modal fade" id="<?= $tabSemaine[$i]?>" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel2"><?= $tabPoseExe['labelAnnonceur'];?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <h4>Vous allez supprimer la pose</h4>
                                            <form method="POST" action="">
                                            <?php include '../php/deletePlanning.php'; ?>
                                                <div class="form-group" id="form-hidden">
                                                    <label class="col-form-label mt-4" for="id">Date de pose</label>
                                                    <input type="text" class="form-control" id="id" name="id" value="<?=$tabPoseExe['idInstallation']?>" required>
                                                </div>
                                                <input type="submit" name="submitDelete" id="connexion" class="btn btn-success" data-bs-dismiss="modal" value="OK">    
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </tr>
                <?php $i=$i+2; endwhile;?>
            </tbody>
        </table>
    </div>
    </div>
</body>
</html>

<script src="../style/jquery.js"></script>
<script src="../vendor/bootstrap.min.js"></script>
<script src="../vendor/axio.min.js"></script>
<script src="../vendor/moment.min.js"></script>