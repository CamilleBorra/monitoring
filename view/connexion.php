<?php require_once '../php/verify.php';
 session_start();?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <link rel="stylesheet" href="../style/pimp.css" type="text/css">
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
<?php include "nav.php";?>
   
    <div class="container">
        <fieldset >
            <legend>Connexion</legend>
            <form id="formulaire" method="POST" action="">
                <div class="form-group">
                    <label class="col-form-label label mt-4" for="login">Login</label>
                    <input type="text" class="form-control" id="login" name="login">
                </div>
                <div class="form-group">
                    <label for="pwd" class="form-label label mt-4">Mot de passe</label>
                    <input type="password" class="form-control" id="pwd" name="pwd">
                </div>
                <div class="envoyer mt-4">
                    <button type="submit" id="connexion" class="btn btn-success" name="submit">Connexion</button>
                </div>
            </form>
        </fieldset>
    </div>
    
</body>
</html>