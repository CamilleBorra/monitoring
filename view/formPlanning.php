<?php require_once '../php/affichage.php';
session_start();
if(!isset($_SESSION) || $_SESSION['connected'] !== 1) {
    header("Location: connexion.php");
    
} 
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../vendor/bootstrap.css" type="text/css"> 
    <link rel="stylesheet" href="../style/pimp.css" type="text/css">
    <link href="../vendor/fontawesome-free-6.1.1-web/css/all.css" rel='stylesheet'> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart Lighting</title>
</head>
<body>
<?php include "nav.php";?>
    <div class="container">
            <fieldset>  
            <legend>Nouvelle pose</legend>
            <form method="POST" action="">
                <?php include '../php/insertPlanning.php'; ?>

                <div class="form-group">
                    <label for="annonceur" class="form-label mt-4 label">Annonceur</label>
                    <select class="form-select" id="annonceur" name="annonceur">
                        <?php while($allAnnonceurExe=$allAnnonceur->fetch()):?> 
                            <option value="<?= $allAnnonceurExe['idAnnonceur'];?>"><?= $allAnnonceurExe['labelAnnonceur'];?></option>
                        <?php endwhile; ?>
                            <option value="newAnnonceur">Ajouter un annonceur</option>
                    </select>
                </div>

                <!-- hidden by default -->
                <div class="form-group" id="form-annonceur">
                    <label class="col-form-label mt-4 label" for="newAnnonceur">Nouvel Annonceur</label>
                    <input type="text" class="form-control" id="newAnnonceur" name="newAnnonceur">
                </div>
                <!-- hidden by default -->

                <div class="form-group">
                    <label for="site" class="form-label mt-4 label">Site</label>
                    <select class="form-select" id="site" name="site">
                        <?php while($recupAllSiteExe=$recupAllSite->fetch()):?> 
                            <option value="<?= $recupAllSiteExe['codeAffaire'];?>"><?= $recupAllSiteExe['nomSite'];?></option>
                        <?php endwhile; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="start">Date de pose</label>
                    <input type="date" class="form-control" id="start" name="start" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="end">Date de dépose</label>
                    <input type="date" class="form-control" id="end" name="end" required>
                </div>

                <div class="form-group">
                    <label class="col-form-label mt-4 label" for="color">Couleur</label>
                    <input data-jscolor="{}" value="#3399FF" name="color" id="color">
                </div> 
                
                <button type="submit" name="submit" id="connexion" class="btn btn-success mt-4">Valider</button>
            </form>
        
        </fieldset>
    </div>
</body>
</html>
<script src="../vendor/jscolor.js"></script>
<script>
    document.querySelector('#annonceur').addEventListener("change", function() {
        const v = this.value;
        const formAnnonceur = document.getElementById('form-annonceur');

        if (v == "newAnnonceur") {
            formAnnonceur.style.display = "block";
            document.getElementById('newAnnonceur').required="required";
        } else {
            formAnnonceur.style.display = "none";
            document.getElementById('newAnnonceur').require=null;
        } 
    });
</script>               

