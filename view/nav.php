
  <div class="sidebar close">
    <ul class="nav-links">
      
    <li>
        <a href="declaration.php">
        <i class="fa-solid fa-flag"></i>
          <span class="link_name">Déclarer un incident</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="declaration.php">Déclarer un incident</a></li>
        </ul>
      </li>

      <li>
        <a href="index.php">
          <i class="fa-solid fa-map"></i>
          <span class="link_name">Carte</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="index.php">Carte</a></li>
        </ul>
      </li>

      <li>
        <a href="alerte.php">
        <i class="fa-solid fa-bell"></i>
          <span class="link_name">Alerte en cours </span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="alerte.php">Alerte en cours</a></li>
        </ul>
      </li>

      <li>
        <a href="planning3.php">
        <i class="fa-solid fa-calendar-days"></i>
          <span class="link_name">Planning</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="planning3.php">Planning</a></li>
        </ul>
      </li>


      <!-- <li>
        <div class="iocn-link">
          <a href="#">
            <i class='bx bx-building-house' ></i>
            <span class="link_name">Sites</span>
          </a>
          <i class='bx bxs-chevron-down arrow' ></i>
        </div>
        <ul class="sub-menu">
          <li><a class="link_name" href="#">Sites</a></li>
          <li><a href="ajouterSite.php">Ajouter un site</a></li>
          <li><a href="modifierSite.php">Modifier un site</a></li>
          <li><a href="supprimerSite.php">Supprimer un site</a></li>
        </ul>
      </li> -->

      <!-- <li>
        <a href="#">
          <i class="fa-solid fa-chart-pie"></i>
          <span class="link_name">Statistique</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="#">Statistique</a></li>
        </ul>
      </li> -->
      
      <li>
        <a href="modifierSite.php">
        <i class="fa-solid fa-sliders"></i>
          <span class="link_name">Modifier un site</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="modifierSite.php">Modifier un site</a></li>
        </ul>
      </li>

      <li>
        <a href="supprimerSite.php">
        <i class="fa-solid fa-trash-can"></i>
          <span class="link_name">Supprimer un site</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="supprimerSite.php">Supprimer un site</a></li>
        </ul>
      </li>



      <?php 
  
      if(!isset($_SESSION) || !isset($_SESSION['connected'])) :?>
        <li>
          <a href="connexion.php">
          <i class="fa-solid fa-user"></i>
            <span class="link_name">Connexion</span>
          </a>
          <ul class="sub-menu blank">
            <li><a class="link_name" href="connexion.php">Se connecter</a></li>
          </ul>
        </li>
        <?php endif; ?>
    
    




  </li>
</ul>
  </div>
  <!-- <section class="home-section">
    <div class="home-content">
      <i class='bx bx-menu' ></i>
      <span class="text">Drop Down Sidebar</span>
    </div>
  </section> -->
  <script>
  let arrow = document.querySelectorAll(".arrow");
  for (var i = 0; i < arrow.length; i++) {
    arrow[i].addEventListener("click", (e)=>{
   let arrowParent = e.target.parentElement.parentElement;//selecting main parent of arrow
   arrowParent.classList.toggle("showMenu");
    });
  }
  let sidebar = document.querySelector(".sidebar");
  let sidebarBtn = document.querySelector(".bx-menu");
  console.log(sidebarBtn);
  sidebarBtn.addEventListener("click", ()=>{
    sidebar.classList.toggle("close");
  });
  </script>