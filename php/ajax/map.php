<?php

    require_once "../../php/affichage.php";

    if(!isset($_GET['agence'])) {
        die(json_encode(array('status' => 'error', 'message' => 'Veuillez renseigner une agence (ville).')));
    }

    $nomAgence=$_GET['agence'];
    $dataAgence=$db->query("SELECT longitude, latitude FROM agence WHERE nameAgence=\"$nomAgence\"");

    $fetchAgence = $dataAgence->fetch(PDO::FETCH_ASSOC);

    $longitude=$fetchAgence['longitude'];
    $lattitude=$fetchAgence['latitude'];
    
    $tabCoordonne=[$lattitude,$longitude];

    die(json_encode($tabCoordonne));