<?php 
require 'bdd.php';

if (isset($_POST['submit'])) {
    $id             = $_POST['site'];
    $nomSite        = $_POST['nomSite'];
    $adresse        = $_POST['adress'];
    $city           = $_POST['city'];
    $lat            = $_POST['lat'];
    $long           = $_POST['long'];
    $start          = $_POST['start'];
    $end            = $_POST['end'];
    $newHabillage   = $_POST['newHabillage'];
    $poseur         = $_POST['poseur'];
    $imprimeur      = $_POST['imprimeur'];

    $update=$db->prepare("UPDATE site SET nomSite             = :nomSite, 
                                          adresse             = :adresse, 
                                          ville               = :ville, 
                                          lattitude           = :lattitude, 
                                          longitude           = :longitude , 
                                          installation        = :installation,
                                          fin                 = :fin,
                                          newHabillage        = :newHabillage,
                                          poseur              = :poseur,
                                          imprimeur           = :imprimeur,
                                          seuilIntensite      = :seuilIntensite,
                                          seuilVent           = :seuilVent,    
                                          seuilLuminositeHaut = :seuilLuminositeHaut,
                                          seuilLuminositeBas  = :seuilLuminositeBas,  
                                          seuilTensionFil     = :seuilTensionFil
                                        WHERE codeAffaire=\"$id\"");

    
    $update -> execute ([
        'nomSite'            => $nomSite,
        'adresse'            => $adresse,
        'ville'              => $city,
        'lattitude'          => $lat, 
        'longitude'          => $long,
        'installation'       => $start,
        'fin'                => $end,
        'newHabillage'       => $newHabillage,
        'poseur'             => $poseur,
        'imprimeur'          => $imprimeur,
        'seuilIntensite'     => !empty($_POST['seuilIntensite'])&& isset($_POST['seuilIntensite']) ? $_POST['seuilIntensite'] : null,
        'seuilVent'          => !empty($_POST['seuilVent']) && isset($_POST['seuilVent']) ? $_POST['seuilVent'] : null,
        'seuilLuminositeHaut'=> !empty($_POST['seuilLumHaut']) && isset($_POST['seuilLumHaut']) ? $_POST['seuilLumHaut'] : null,
        'seuilLuminositeBas' => !empty($_POST['seuilLumBas']) && isset($_POST['seuilLumBas']) ? $_POST['seuilLumBas'] : null,
        'seuilTensionFil'    => !empty($_POST['seuilTensionFil']) && isset($_POST['seuilTensionFil']) ? $_POST['seuilTensionFil'] : null,
    ]);
}


