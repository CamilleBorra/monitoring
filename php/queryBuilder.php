<?php
interface QueryBuilder{
    public function select(string $table, array $columns) : QueryBuilder;
    public function insert(string $table, array $values) : QueryBuilder;
    public function where(string $column, string $value, string $operator = '=') : QueryBuilder;
    public function limit(int $from,int $offset):QueryBuilder;
    public function getQuery(): string;
}

class MysqlBuilder implements QueryBuilder{
    private $query;
    private function reset(){
        $this->query = new \stdClass;
    }
    public function select(string $table,array $columns):QueryBuilder
    {
        $this->reset();
        $this->query->base="SELECT ". implode(', ',$columns)." FROM ".$table." ";
        return $this;
    }

    public function insert(string $table,array $values):QueryBuilder
    {
        $this->query->base="INSERT INTO ".$table." VALUES (".implode(", ",$values).")";
        return $this;
    }

    public function where(string $column, string $value, string $operator="="):QueryBuilder
    {
        $this->query->where[]=$column.$operator."'".$value."'";
        return $this;
    }

    public function limit(int $from,int $offset):QueryBuilder
    {
        $this->query->limit=" LIMIT ".$from.", ".$offset;
        return $this;
    }

    public function getQuery(): string
    {
        $query = $this->query;
        $sql = $query->base;
        if(!empty($query->where)){
            $sql .= ' WHERE '.implode(' AND ', $query->where);
        }
        if(isset($query->limit)){
            $sql .= $query->limit;
        }
        
        $sql .= ';';
        return $sql;
    }


}

