<?php

    require_once '../php/affichage.php';

    $tabSite = [];
    $now=date('Y-m-d H:i:s');
    
    while($recupAllSiteExe = $recupAllSite->fetch()) {
        // var_dump($recupAllSiteExe['nomSite']);
        $recupEtat = $db->query("SELECT * FROM probleme p, categorie c WHERE c.idCategorie=p.idCategorie AND p.codeAffaire=".$recupAllSiteExe['codeAffaire']." AND p.reparation IS NULL");
        $recupLog = $db->query("SELECT * FROM log WHERE codeAffaire=".$recupAllSiteExe['codeAffaire']." ORDER BY idLog DESC");
        if($recupAllSiteExe['installation']>$now){
            
            $debut=explode(' ',$recupAllSiteExe['installation']);
            $fin=explode(' ',$recupAllSiteExe['fin']);
            $tabProbleme= array(
                'nom' => $recupAllSiteExe['nomSite'],
                'longitude' => $recupAllSiteExe['longitude'],
                'lattitude' => $recupAllSiteExe['lattitude'],
                'fin'=> $fin[0],
                'debut'=> $debut[0], 
            );
    
        }
        else{ 
            $recupEtatExe = $recupEtat->fetchAll();
            for($i = 0; $i < 1; $i++) {
           
            if($recupEtat->rowCount() === 0) {
            $debut=explode(' ',$recupAllSiteExe['installation']);
            $fin=explode(' ',$recupAllSiteExe['fin']);
            
            $tabProbleme = array(
                'nom' => $recupAllSiteExe['nomSite'],
                'categorie' => 'ok',
                'longitude' => $recupAllSiteExe['longitude'],
                'lattitude' => $recupAllSiteExe['lattitude'],
                'fin'=> $fin[0],
                'debut'=> $debut[0], 
            );
        }

        else{
            

            
                $debut=explode(' ',$recupAllSiteExe['installation']);
                $fin=explode(' ',$recupAllSiteExe['fin']);

                $tabProbleme = array(
                    'nom' => $recupAllSiteExe['nomSite'],
                    'categorie' => $recupEtatExe[$i]['type'],
                    'flag' => $recupEtatExe[$i]['flag'],
                    'longitude' => $recupAllSiteExe['longitude'],
                    'lattitude' => $recupAllSiteExe['lattitude'],
                    'fin'=> $fin[0],
                    'debut'=> $debut[0],                
                );
          
        }  
        }
            $recupLogExe = $recupLog->fetchAll();
            for($i = 0; $i < 1; $i++) {
                $tabLog= array(
                'intensite' => $recupLogExe[$i]['intensiteLog'],
                'vent' => $recupLogExe[$i]['ventLog'],
                );
                array_push($tabProbleme, $tabLog);
            }

        
        
        }
        array_push($tabSite, $tabProbleme); 
    }
    die(json_encode($tabSite));