<?php 
require 'bdd.php';

if (isset($_POST['submit']))
{   
    $nom=$_POST['site'];
    $adresse=$_POST['adress'];
    $city=$_POST['city'];
    $agence=$_POST['agence'];
    $long=$_POST['long'];
    $lat=$_POST['lat'];
    $start=$_POST['start'];
    $end=$_POST['end'];
    $poseur=$_POST['poseur'];
    $imprimeur=$_POST['imprimeur'];

    if (isset ($_FILES["images"]["name"]) && !empty($_FILES["images"]["name"]) )
    {
        $target_dir = "../imageSite/";
        $namePicture=basename($_FILES["images"]["name"]);
        $explode = explode('.', $namePicture);
        $extension = $explode[count($explode) - 1];
        $extension=strtolower($extension);
        $nameWithExtension=time().".".$extension;

        $target_file = $target_dir.$nameWithExtension ;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image images is a actual image or fake image
        if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["images"]["tmp_name"]);
        if($check !== false) {
            
            $uploadOk = 1;
        } else {
            
            $uploadOk = 0;
        }
        }
        move_uploaded_file($_FILES["images"]["tmp_name"], $target_file);
            $im=$nameWithExtension;
        $insertProbleme=$db->prepare("INSERT INTO site (nomSite, adresse, ville, urlImage, installation, longitude, lattitude, fin, newHabillage, idAgence, poseur, imprimeur, seuilIntensite, seuilTensionFil, seuilLuminositeHaut, seuilLuminositeBas, seuilVent) Values (:nomSite, :adresse, :ville, :urlImage, :installation, :longitude, :lattitude, :fin, :newHabillage, :idAgence, :poseur, :imprimeur,  :seuilIntensite, :seuilTensionFil, :seuilLuminositeHaut, :seuilLuminositeBas, :seuilVent)");
        $insertProbleme -> execute ([
            'nomSite'=>$nom,
            'adresse'=>$adresse, 
            'ville'=>$city, 
            'urlImage'=>$im, 
            'installation'=>$start, 
            'longitude'=>$long, 
            'lattitude'=>$lat, 
            'fin'=>$end, 
            'newHabillage'=>$start, 
            'idAgence'=>$agence, 
            'poseur'=>$poseur, 
            'imprimeur'=>$imprimeur,
            'seuilIntensite'=> !empty($_POST['seuilIntensite'])&& isset($_POST['seuilIntensite']) ? $_POST['seuilIntensite'] : null, 
            'seuilTensionFil'=> !empty($_POST['seuilTensionFil']) && isset($_POST['seuilTensionFil']) ? $_POST['seuilTensionFil'] : null,
            'seuilLuminositeHaut'=> !empty($_POST['seuilLumHaut']) && isset($_POST['seuilLumHaut']) ? $_POST['seuilLumHaut'] : null, 
            'seuilLuminositeBas'=> !empty($_POST['seuilLumBas']) && isset($_POST['seuilLumBas']) ? $_POST['seuilLumBas'] : null, 
            'seuilVent'=>!empty($_POST['seuilVent']) && isset($_POST['seuilVent']) ? $_POST['seuilVent'] : null,
        
        ]);?>
        <div class="alert alert-dismissible alert-success">
        <strong>Site enregistré !</strong>
        </div>
        <?php
    }
}

