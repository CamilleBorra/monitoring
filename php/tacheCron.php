<?php
    require 'bdd.php';
    require 'emailMonitoring.php';
    $today=new DateTime('NOW');

    $recupNonOuvert=$db->query("SELECT idProbleme, dateProbleme, position  FROM probleme WHERE envoie=0 ");
    while($recupNonOuvertExe=$recupNonOuvert->fetch()){

        $dateRecup= $recupNonOuvertExe['dateProbleme'];
        $dateRecup= new DateTime($dateRecup);

        $diff= $dateRecup->diff($today);
        $diff=$diff->format("%I");

        if($diff>1 && $recupNonOuvertExe['position']<=4)
        {
            envoyer($recupNonOuvertExe['idProbleme'],$recupNonOuvertExe['position'],$db);
        }
    }

?>